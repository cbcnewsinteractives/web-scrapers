const p = require('puppeteer'); //Duh.
const fs = require('fs'); //Saves csv file to local drive.
const { facebook } = require('./links.js'); //Saves csv file to local drive.

// Running this requires you to login to your Facebook account the first time you run it. Then, close the browser and the cookies should save.
// Run the script again and it should work.

async function runFacebook() {
  return new Promise(async (resolve, reject) => {
      const cookiesPath = "cookies.txt";
      // If the cookies file exists, read the cookies.
      const previousSession = fs.existsSync(cookiesPath)
      if (previousSession) {
        const content = fs.readFileSync(cookiesPath);
        const cookiesArr = JSON.parse(content);
        if (cookiesArr.length !== 0) {
          for (let cookie of cookiesArr) {
            await page.setCookie(cookie)
          }
          console.log('Session has been loaded in the browser')
        }
      }

      const browser = await p.launch({
        headless: false,
        defaultViewport: null,
        slowMo: 0,
        args: ["--user-data-dir=./Google/Chrome/User Data/"],
      });
      const page = await browser.newPage();
      // Write Cookies
      const cookiesObject = await page.cookies()
      fs.writeFileSync(cookiesPath, JSON.stringify(cookiesObject));
      console.log('Session has been saved to ' + cookiesPath);
      for (pageNum = 0; pageNum < facebook.length; pageNum++) {
        await page.goto("https://www.facebook.com/" + facebook[pageNum]);

        for (i = 3; i < 5; i++) {
          const selector = '.k4urcfbm.dp1hu0rb.d2edcug0.cbu4d94t.j83agx80.bp9cbjyn > .k4urcfbm:nth-child(' + i + ')';
          await page.waitForSelector(selector)

          var position = await page.evaluate(selector => {
            //This function gets position of element to screenshot
            function offset(el) {
          	    var rect = el.getBoundingClientRect(),
          	    scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
          	    scrollTop = window.pageYOffset || document.documentElement.scrollTop;
          	    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
          	}

            const element = document.querySelector(selector);
            const divOffset = offset(element);
            console.log(divOffset.left, divOffset.top);
            window.scroll(0, divOffset.top-120)

            var elementHeight = element.offsetHeight
            var elementWidth = element.offsetWidth
            return {width: elementWidth, height: elementHeight, topOffset: divOffset.top, leftOffset: divOffset.left}
          }, selector);
          if (position.height > 15) {
            let num = 1
            await page.screenshot({
              path: './screenshots/facebook-' + facebook[pageNum] + num + '.png',
              clip: {
                x: position.leftOffset,
                y: position.topOffset,
                width: position.width,
                height: position.height,
              }
            })
            num++;
          }
        }
      }
  })
}

module.exports.runFacebook = runFacebook
