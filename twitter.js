const p = require('puppeteer'); //Duh.
const fs = require('fs'); //Saves csv file to local drive.
let { twitter } = require('./links.js')

async function runTwitter() {
  return new Promise(async (resolve, reject) => {
      const browser = await p.launch({
        headless: false,
        defaultViewport: null,
        slowMo: 200
      });
      const page = await browser.newPage();
      for (pageNum = 0; pageNum < twitter.length; pageNum++) {
        console.log(pageNum)
          await page.goto("https://twitter.com/" + twitter[pageNum]);

          for (i = 3; i < 5; i++) {
            const selectorParent = '.r-1ljd8xs > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > section:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div';
            const selector = '.r-1ljd8xs > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > section:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(' + i + ')';
            await page.waitForSelector(selectorParent)

            var position = await page.evaluate(selector => {
              //This function gets position of element to screenshot
              function offset(el) {
            	    var rect = el.getBoundingClientRect(),
            	    scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
            	    scrollTop = window.pageYOffset || document.documentElement.scrollTop;
            	    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
            	}

              const element = document.querySelector(selector);
              const divOffset = offset(element);
              console.log(divOffset.left, divOffset.top);
              window.scroll(0, divOffset.top-50)

              var elementHeight = element.offsetHeight
              var elementWidth = element.offsetWidth
              return {width: elementWidth, height: elementHeight, topOffset: divOffset.top, leftOffset: divOffset.left}
            }, selector);
            if (position.height > 15) {
              let num = 1
              await page.screenshot({
                path: './screenshots/twitter-' + twitter[pageNum] + num + '.png',
                clip: {
                  x: position.leftOffset,
                  y: position.topOffset,
                  width: position.width,
                  height: position.height,
                }
              })
              num++
            }
          }
      }
      browser.close()
  })
}

module.exports.runTwitter = runTwitter
